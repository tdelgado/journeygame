//
//  JGCustomScene.h
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 26/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface JGCustomScene : SKScene

@property(nonatomic) int adventureType;

-(id)initWithSize:(CGSize)size andItens:(NSArray*)question backGround:(NSString*) backGround;

@end
