//
//  JGTracking.m
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 27/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import "JGTracking.h"

@implementation JGTracking

// ADVENTURE
NSString * const ONE = @"ONE";
NSString * const TWO = @"TWO";
// PHASE
NSString *const PHASE_MARS = @"MARS";
NSString *const PHASE_JUPITER = @"JUPITER";
NSString *const PHASE_EARTH = @"EARTH";
NSString *const PHASE_SAPTURN = @"SAPTURN";
NSString *const PHASE_VENUS = @"VENUS";
NSString *const PHASE_NEPTUNE = @"NEPTUNE";


#pragma mark - Singleton
+(id)shareTrackingInstance
{
    static JGTracking *jgTracking = nil;
    static dispatch_once_t onceToken=0;
    dispatch_once(&onceToken, ^{
        jgTracking = [[super allocWithZone:nil] init];
        jgTracking.adventure = nil;
        jgTracking.phase = nil;
    });
    
    return jgTracking;
}


+(id)allocWithZone:(struct _NSZone *)zone
{
    return [self shareTrackingInstance];
}





@end
