//
//  JGQuestion.m
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 26/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import "JGQuestion.h"

@interface JGQuestion ()

@end


@implementation JGQuestion


-(id) initQuestionWithAsk:(NSString*)ask
                   choose:(NSArray*) choose
                   answer:(NSString*)answer
{
    self = [super init];
    if (self)
    {
        _ask = [ask copy];
        _choose = [choose copy];
        _answer = [answer copy];
        _right = NO;
    }
    return self;
}


-(BOOL) pickAnswer:(NSString*) answer
{
    BOOL isRight = NO;
    if ([_choose containsObject:answer])
    {
        isRight = YES;
    }
    return isRight;
    
}


@end
