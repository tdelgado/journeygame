//
//  JGViewController.h
//  Journey Game
//

//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface JGViewController : UIViewController

@end
