//
//  JGFactoryItem.h
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 27/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface JGFactoryItem : NSObject

// Adventure ONe

+(SKSpriteNode*) bobNodeMars;
+(SKSpriteNode*) bobNodeSaturn;
+(SKSpriteNode*) bobNodeSaturn2;
+(SKSpriteNode*) bobNodeJupiter;
+(SKSpriteNode*) bobNodeVenus;
+(SKSpriteNode*) bobNodeEarth;


// Adventure Two

+(SKSpriteNode*) miguelitoNodeNeptune;
+(SKSpriteNode*) miguelitoNodeSaturn;
+(SKSpriteNode*) miguelitoNodeJupiter;
+(SKSpriteNode*) miguelitoNodeMars;
+(SKSpriteNode*) miguelitoNodeEarth;

@end
