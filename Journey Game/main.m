//
//  main.m
//  Journey Game
//
//  Created by Thomas Delgado on 4/21/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JGAppDelegate class]));
    }
}
