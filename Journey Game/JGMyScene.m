//
//  JGMyScene.m
//  Journey Game
//
//  Created by Thomas Delgado on 4/21/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "JGMyScene.h"
#import "JGAdventureOne.h"
#import "JGAdventureTwo.h"
#import "JGTracking.h"

@interface JGMyScene ()

@property(nonatomic) AVAudioPlayer *audioPlayer;
@property(nonatomic) JGTracking *tracking;

@end


@implementation JGMyScene


#pragma mark - Create SKLabelNode
-(SKLabelNode*) createLabelNodeWithTitle:(NSString*)title
                                fontSize:(NSInteger)fontSize
                               positionX:(float)x
                               positionY:(float)y
                                fontName:(NSString*)fontName
                                nameNode:(NSString*)name
{
    SKLabelNode *node = [SKLabelNode labelNodeWithFontNamed:fontName];
    node.text = title;
    node.fontSize = fontSize;
    node.position = CGPointMake(x, y);
    node.name = name;
    return node;
}


-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
     
        self.backgroundColor = [SKColor blackColor];
     
        NSURL *musicURL = [[NSBundle mainBundle] URLForResource:@"Tinkle" withExtension:@"mp3"];
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicURL error:nil];
        _audioPlayer.numberOfLoops = -1;
        [_audioPlayer prepareToPlay];
        [_audioPlayer play];
        
        // Sky
        NSString *skyPath = [[NSBundle mainBundle] pathForResource:@"Sky" ofType:@"sks"];
        SKEmitterNode *emmiterSky =[NSKeyedUnarchiver unarchiveObjectWithFile:skyPath];
        emmiterSky.zPosition = -1.0;
        
        // Create all SKLabelNodes
        SKLabelNode *title = [self createLabelNodeWithTitle:@"Journey Game"
                                                   fontSize:40
                                                  positionX:CGRectGetMidY(self.frame) / 1.35
                                                  positionY:CGRectGetMidX(self.frame) / .65
                                                   fontName:@"Chalkduster"
                                                   nameNode:@"TITLE"];
        
        SKLabelNode *one = [self createLabelNodeWithTitle:@"Adventure One"
                                                 fontSize:30
                                                positionX:CGRectGetMidX(self.frame)
                                                positionY:CGRectGetMidY(self.frame) / 1.1
                                                 fontName:@"Chalkduster"
                                                 nameNode:@"ONE"];
        
        
        SKLabelNode *two = [self createLabelNodeWithTitle:@"Adventure Two"
                                                          fontSize:30
                                                positionX:CGRectGetMidX(self.frame)
                                                positionY:CGRectGetMidY(self.frame) / 1.3 fontName:@"Chalkduster"
                                                 nameNode: @"TWO"];
        // Add to the scene
        [self addChild:title];
        [self addChild:one];
        [self addChild:two];
        [self addChild:emmiterSky];
        
        
        // Start tracking
        self.tracking = [JGTracking shareTrackingInstance];
        self.tracking.adventure =  nil;
        self.tracking.adventure = nil;
        
    }
    return self;
}




-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // Go to adverture ONE
    if ([node.name isEqualToString:@"ONE"])
    {
        // Set adventure e start phase
        self.tracking.adventure  = ONE;
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
        JGAdventureOne *newScene = [[JGAdventureOne alloc] initWithSize: self.frame.size];
        newScene.scaleMode = SKSceneScaleModeAspectFill;

        // Another configurations in the new scene
        [self.view presentScene:newScene transition:reveal];
    }
    
    // Go to adverture TWO
    if ([node.name isEqualToString:@"TWO"])
    {
        // Set adventure e start phase
        self.tracking.adventure = TWO;
        SKTransition *reveal = [SKTransition flipVerticalWithDuration:2.0];
        JGAdventureTwo *newScene = [[JGAdventureTwo alloc] initWithSize: self.frame.size];
        newScene.scaleMode = SKSceneScaleModeAspectFill;

        // Another configurations in the new scene
        [self.view presentScene:newScene transition:reveal];
    }


}

-(void)update:(CFTimeInterval)currentTime
{
   
    

}

@end
