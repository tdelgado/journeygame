//
//  JGAdventureTwo.m
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 21/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import "JGAdventureTwo.h"
#import "JGMyScene.h"
#import "JGTracking.h"
#import "JGCustomScene.h"
#import "JGFactoryItem.h"

@interface JGAdventureTwo ()

@property(nonatomic) JGTracking *tracking;
@property(nonatomic) JGCustomScene *customScene;

@end


@implementation JGAdventureTwo


-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
    {
        self.tracking = [JGTracking shareTrackingInstance];
        self.backgroundColor = [SKColor blackColor];

        if (self.tracking.phase == nil)
        {
            self.tracking.phase = PHASE_NEPTUNE;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem miguelitoNodeNeptune] ]
                                                        backGround:@"miguelitos0046.jpg"];
        }else if ( [self.tracking.phase isEqualToString:PHASE_NEPTUNE])
        {
            self.tracking.phase = PHASE_SAPTURN;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem miguelitoNodeSaturn]]
                                                        backGround:@"miguelitos0045.jpg"];
            
        }else if ( [self.tracking.phase isEqualToString:PHASE_SAPTURN] )
        {
            self.tracking.phase = PHASE_JUPITER;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem miguelitoNodeJupiter]]
                                                        backGround:@"miguelitos0044.jpg"];
        }else if ( [self.tracking.phase isEqualToString:PHASE_JUPITER] ){
            self.tracking.phase = PHASE_MARS;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem miguelitoNodeMars]]
                                                        backGround:@"miguelitos0043.jpg"];
        }else if ( [self.tracking.phase isEqualToString:PHASE_MARS] ){
            self.tracking.phase = PHASE_EARTH;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem miguelitoNodeEarth]]
                                                        backGround:@"miguelitos0042.jpg" ];
            
        }else if ( [self.tracking.phase isEqualToString:PHASE_EARTH] ){
            NSLog(@"COME BACK TO INTRO");
            self.tracking.phase = nil;
            self.tracking.adventure = nil;
            
        }
        
    }
    return self;
}



//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    CGPoint location = [touch locationInNode:self];
//    SKNode *node = [self nodeAtPoint:location];
//    
//    
//    if ([node.name isEqualToString:@"TWO"])
//    {
//        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
//        JGMyScene *newScene = [[JGMyScene alloc] initWithSize: self.frame.size];
//        newScene.scaleMode = SKSceneScaleModeAspectFill;
//        // Another configurations in the new scene
//        [self.view presentScene:newScene transition:reveal];
//    }
//}



-(void)update:(CFTimeInterval)currentTime
{
    
    SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
    if (self.tracking.adventure == nil)
    {
        JGMyScene *scene =  [[JGMyScene alloc] initWithSize:self.frame.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        [self.view presentScene:scene transition:reveal];
    }
    else
    {
        self.customScene.scaleMode = SKSceneScaleModeAspectFill;
        self.customScene.adventureType = 2;
        [self.view presentScene:self.customScene transition:reveal];
    }
    
}




@end
