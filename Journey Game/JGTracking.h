//
//  JGTracking.h
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 27/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

// Type adventure
extern NSString *const ONE;
extern NSString *const TWO;

// Type phase
extern NSString *const PHASE_MARS;
extern NSString *const PHASE_JUPITER;
extern NSString *const PHASE_EARTH;
extern NSString *const PHASE_SAPTURN;
extern NSString *const PHASE_VENUS;
extern NSString *const PHASE_NEPTUNE;


@interface JGTracking : NSObject

@property(nonatomic) NSString *adventure;
@property(nonatomic) NSString *phase;


+(id)shareTrackingInstance;



@end
