//
//  JGFactoryItem.m
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 27/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//


// ALLOCA TODOS OS SPRITES DO GAME

#import "JGFactoryItem.h"
#import "JGQuestion.h"

@implementation JGFactoryItem


// Adventure ONe


+(SKSpriteNode*) bobPresent
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.name = @"BOBPRESENT";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Who step on the moon for the first time?"
                                                            choose:@[@"Neil Armstrong", @"Galileu Galilei", @"Nosa"]
                                                            answer:@"Neil Armstrong"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) bobNodeMars
{
    
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"BOBMARS";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Who step on the moon for the first time?"
                                                     choose:@[@"Neil Armstrong", @"Galileu Galilei", @"Nosa"]
                                                     answer:@"Neil Armstrong"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) bobNodeSaturn
{
   
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"BOBSATURN";
    
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"How many planets are in the solar system?"
                                                            choose:@[@"11", @"8", @"7"]
                                                            answer:@"8"];
    
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) bobNodeSaturn2
{

    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"BOBSATURN2";
    
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"What is the biggest planet?"
                                                            choose:@[@"Éris", @"Earth", @"Jupiter"]
                                                            answer:@"Jupiter"];
    
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}




+(SKSpriteNode*) bobNodeJupiter
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"BOBOJUPITER";
    
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Bob's tank has 120 liters,he used 2/3.How many liters did he use?"
                                                            choose:@[@"60", @"40", @"80"]
                                                            answer:@"80"];
    
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) bobNodeVenus
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"BOBVENUS";
    
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Unscramble the World: esrevinU?"
                                                            choose:@[@"Universe", @"Uniserve", @"Uvenirse"]
                                                            answer:@"Universe"];
    
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) bobNodeEarth
{

    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"bob will's journey0025.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"BOBEARTH";
    
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Unscramble the World: esrevinU?"
                                                            choose:@[@"Universe", @"Uniserve", @"Uvenirse"]
                                                            answer:@"Universe"];
    
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}


// Adventure Two

+(SKSpriteNode*) miguelitoNodeNeptune
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"miguelitos0033.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"MEGUELITONPETUNE";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"How many moons does Neptune have?"
                                                            choose:@[@"3", @"20", @"13"]
                                                            answer:@"13"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) miguelitoNodeSaturn
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"miguelitos0029.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"MEGUELITOSATURN";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Whats the color of Saturn?"
                                                            choose:@[@"Yellow", @"Orange", @"Green"]
                                                            answer:@"Yellow"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}


+(SKSpriteNode*) miguelitoNodeJupiter
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"miguelitos0031.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"MEGUELITOJUPITER";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Who is not a Beatle?"
                                                            choose:@[@"Ringo Star", @"George Harisson", @"Mick Jeager"]
                                                            answer:@"Mick Jeager"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}

+(SKSpriteNode*) miguelitoNodeMars
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"miguelitos0030.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"MEGUELITOSMARS";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Whats the position of Mars in the Solar System?"
                                                            choose:@[@"1", @"4", @"2"]
                                                            answer:@"4"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;

}

+(SKSpriteNode*) miguelitoNodeEarth
{
    SKSpriteNode *node = [[SKSpriteNode alloc] initWithImageNamed:@"miguelitos0022.png"];
    node.size = CGSizeMake(50, 50);
    
    node.name = @"MEGUELITOSEARTH";
    JGQuestion *question = [[JGQuestion alloc] initQuestionWithAsk:@"Whats the most populated contry in the world?"
                                                            choose:@[@"China", @"USA", @"India"]
                                                            answer:@"China"];
    node.userData = [[NSMutableDictionary alloc] init];
    [node.userData setObject:question forKey:@"QUESTION"];
    [node.userData  setObject:@"FINISH" forKey:@"NO"];
    return node;
}



@end
