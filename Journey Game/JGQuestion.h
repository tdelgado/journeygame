//
//  JGQuestion.h
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 26/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JGQuestion : NSObject


@property(nonatomic, readonly, getter = isRight) BOOL right;

@property(nonatomic) NSString *ask;
@property(nonatomic) NSArray *choose;
@property(nonatomic) NSString *answer;


// Inicializador
-(id) initQuestionWithAsk:(NSString*)ask
                   choose:(NSArray*) choose
                   answer:(NSString*)answer;



-(BOOL) pickAnswer:(NSString*) answer;

@end
