//
//  JGModel.h
//  Journey Game
//
//  Created by Thomas Delgado on 4/21/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JGLevel.h"
#import "JGQuestions.h"

@interface JGModel : NSObject

+(void) createPhasesAndQuestions;

@end
