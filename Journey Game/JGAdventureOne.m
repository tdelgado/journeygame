//
//  JGAdventureOne.m
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 21/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

// TRANSITION SCREEN


#import "JGAdventureOne.h"
#import "JGMyScene.h"
#import "JGQuestion.h"
#import "JGFactoryItem.h"
#import "JGTracking.h"
#import "JGCustomScene.h"

@interface JGAdventureOne ()

@property(nonatomic) JGTracking *tracking;
@property(nonatomic) JGCustomScene *customScene;

@end

@implementation JGAdventureOne


-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
    {
        
        self.backgroundColor = [SKColor blackColor];
        
        self.tracking = [JGTracking shareTrackingInstance];
        
        if (self.tracking.phase == nil)
        {
            self.tracking.phase = PHASE_MARS;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem bobNodeMars] ]
                                                        backGround:@"bob will's journey0039.jpg"];
        }else if ( [self.tracking.phase isEqualToString:PHASE_MARS])
        {
            self.tracking.phase = PHASE_SAPTURN;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem bobNodeSaturn],
                                                                      [JGFactoryItem bobNodeSaturn2]
                                                                    ]
                                                        backGround:@"bob will's journey0037.jpg"];
            
        }else if ( [self.tracking.phase isEqualToString:PHASE_SAPTURN] )
        {
            self.tracking.phase = PHASE_JUPITER;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem bobNodeJupiter]]
                                                        backGround:@"bob will's journey0036.jpg"];
        }else if ( [self.tracking.phase isEqualToString:PHASE_JUPITER] ){
            self.tracking.phase = PHASE_VENUS;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem bobNodeVenus]]
                                                        backGround:@"bob will's journey0035.jpg"];
        }else if ( [self.tracking.phase isEqualToString:PHASE_VENUS] ){
            self.tracking.phase = PHASE_EARTH;
            self.customScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                          andItens:@[ [JGFactoryItem bobNodeEarth]]
                                                        backGround:@"bob will's journey0032.jpg" ];
            
        }else if ( [self.tracking.phase isEqualToString:PHASE_EARTH] ){
            NSLog(@"COME BACK TO INTRO");
            self.tracking.phase = nil;
            self.tracking.adventure = nil;
            
        }
        
    }
    return self;
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}



-(void)update:(CFTimeInterval)currentTime
{
    SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
    if (self.tracking.adventure == nil)
    {
        JGMyScene *scene =  [[JGMyScene alloc] initWithSize:self.frame.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        [self.view presentScene:scene transition:reveal];
    }
    else
    {
        self.customScene.scaleMode = SKSceneScaleModeAspectFill;
        self.customScene.adventureType = 1;
        [self.view presentScene:self.customScene transition:reveal];
    }
    
    
    
 /*
    
    if ([self.tracking.phase isEqualToString: PHASE_MARS])
    {
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
        JGCustomScene *newScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                             andItens:@[
                                                                        [JGFactoryItem bobNodeMars],
                                                                        [JGFactoryItem bobNodeSaturn]
                                                                        ]];
        newScene.scaleMode = SKSceneScaleModeAspectFill;
        // Another configurations in the new scene
        [self.view presentScene:newScene transition:reveal];
    }
    
    if ([self.tracking.phase isEqualToString: PHASE_SAPTURN])
    {
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
        JGCustomScene *newScene = [[JGCustomScene alloc] initWithSize:self.frame.size
                                                             andItens:@[
                                                                        [JGFactoryItem bobNodeMars],
                                                                        [JGFactoryItem bobNodeSaturn]
                                                                        ]];
        newScene.scaleMode = SKSceneScaleModeAspectFill;
        // Another configurations in the new scene
        [self.view presentScene:newScene transition:reveal];
    }
    
  
*/
  
}



@end
