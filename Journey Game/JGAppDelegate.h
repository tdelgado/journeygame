//
//  JGAppDelegate.h
//  Journey Game
//
//  Created by Thomas Delgado on 4/21/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
