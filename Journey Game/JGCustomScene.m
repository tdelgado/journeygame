//
//  JGCustomScene.m
//  Journey Game
//
//  Created by Rodrigo Freitas Leite on 26/04/14.
//  Copyright (c) 2014 Thomas Delgado. All rights reserved.
//

#import "JGCustomScene.h"
#import "JGQuestion.h"
#import "JGTracking.h"
#import "JGAdventureOne.h"
#import "JGAdventureTwo.h"

@interface JGCustomScene ()

@property(nonatomic) NSArray *question;
@property(nonatomic) SKNode *questionNode;
@property(nonatomic) int decided;

@property(nonatomic) SKSpriteNode *backGround;

@end


@implementation JGCustomScene


-(id)initWithSize:(CGSize)size andItens:(NSArray*)question backGround:(NSString*) backGround
{
    if (self = [super initWithSize:size])
    {
        self.backgroundColor = [SKColor blackColor];
        
        self.backGround= [[SKSpriteNode alloc] initWithImageNamed:backGround];
        [self addChild:self.backGround];
        self.backGround.xScale = 0.8;
        self.backGround.yScale = 0.75;
        self.backGround.position = CGPointMake(size.width/2, size.height/2);
        
        _question = question;
        int i = 0;
        // for each question (sprite) to the scene
        for (SKSpriteNode *node in _question)
        {
            float x =  arc4random_uniform(self.frame.size.width/3);
            float y =  400; //arc4random_uniform(self.frame.size.height/3);
            [self addChild:node];
            node.name = @"QUEST";
            node.position = CGPointMake(x, y);
            i++;
        }
        
        // Node to Show Question
        self.questionNode = [[SKSpriteNode alloc] initWithColor:[SKColor blackColor]
                                                           size:CGSizeMake(CGRectGetWidth(self.frame) ,
                                                                           CGRectGetHeight(self.frame) / 3)];
        self.questionNode.name = @"QUESTION_NODE";
        self.questionNode.position = CGPointMake(self.frame.size.width / 2, -200.0);
        self.questionNode.xScale = 0.8;
        [self addChild:self.questionNode];
        
        // Control variable
        self.decided = 0;
    
    }
    return self;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"QUEST"])
    {
        [self createQuestionNode: node];
    }
    
    // Respota dada pelo usuario
    if ([node.name isEqualToString:@"QUESTION_RESPONSE"])
    {
        [self.questionNode runAction:[SKAction moveTo:CGPointMake(self.frame.size.width /2, 0) duration:1.0]];
        SKLabelNode *n = (SKLabelNode*) node;
        SKNode *nodeItem = self.questionNode.userData[@"SelectItem"];
        if ([[nodeItem.userData[@"QUESTION"] answer] isEqualToString:n.text])
        {
            NSLog(@"Resposta Certa: %@",(SKLabelNode*)n.text);
            [self addDecidedQuestion:nodeItem];
        }
        else
        {
            NSLog(@"Resposta Errada");
        }
    }
    
    
    // Presentation node, qualquer lugar que tocar sai da tela
    
}



-(void)update:(CFTimeInterval)currentTime
{
    
 // verificar se todas as questions ja acabaram
    if (self.decided == [self.question count])
    {
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:2.0];
        if (self.adventureType == 1)
        {
          JGAdventureOne *newScene = [[JGAdventureOne alloc] initWithSize: self.frame.size];
          newScene.scaleMode = SKSceneScaleModeAspectFill;
          [self.view presentScene:newScene transition:reveal];
           self.decided += 1;
        }else if (self.adventureType == 2)
            {
                JGAdventureTwo *newScene = [[JGAdventureTwo alloc] initWithSize: self.frame.size];
                newScene.scaleMode = SKSceneScaleModeAspectFill;
                [self.view presentScene:newScene transition:reveal];
                self.decided += 1;
            }
    }
    
}

-(void) addDecidedQuestion:(SKNode*)node
{
    node.name = @"";
   [ node runAction:[SKAction moveTo:CGPointMake( (self.decided + 1) * (node.frame.size.width + node.frame.size.width/2),
                                                 self.frame.size.height/1.4 )
                            duration:2.0] completion:^{
       self.decided += 1;
   }];
    
}


-(void) createQuestionNode:(SKNode*) node
{
    JGQuestion *question = node.userData[@"QUESTION"];
    [self.questionNode removeAllChildren];
    SKLabelNode *titleQuestion = [[SKLabelNode alloc] initWithFontNamed:@"Chalkduster"];
    titleQuestion.text =   question.ask;
    [self.questionNode addChild:titleQuestion];
    titleQuestion.fontColor = [SKColor redColor];
    titleQuestion.fontSize = 22;
    titleQuestion.position = CGPointMake(0, 0);
    for (int i = 0; i < [question.choose count]; i++)
    {
        SKLabelNode *choose = [[SKLabelNode alloc] initWithFontNamed:@"Chalkduster"];
        choose.text = question.choose[i];
        choose.name = @"QUESTION_RESPONSE";
        [self.questionNode addChild:choose];
        choose.position = CGPointMake(0, (i+1) * -50);
        choose.fontColor = [SKColor yellowColor];
        choose.fontSize = 17;
    }
    
    self.questionNode.userData = [[NSMutableDictionary alloc] init];
    [self.questionNode.userData setObject:node forKey:@"SelectItem"];
    [self.questionNode runAction:[SKAction moveTo:CGPointMake(self.frame.size.width /2, self.frame.size.height / 1.8) duration:1.0]];
}


@end
